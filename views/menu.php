
<nav class="navbar navbar-expand-lg navbar-light bg-bakamome">
  <a class="navbar-brand brandName" href="#">
    <img src="imgs/mascotte.png" width="70" height="70" class="d-inline-block align-middle mascotte" alt="">
    BAKAMOME
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class=" d-lg-flex align-items-end flex-column collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ">
      <li class="nav-item mr-auto">
        <a class="nav-link menuLink" href="index.php">Home</a>
      </li>
      <li class="nav-item mr-auto">
        <a class="nav-link menuLink" href="games.php" >Games</a>
      </li>
      <li class="nav-item mr-auto">
        <a class="nav-link menuLink" href="https://www.twitch.tv/bakamome" target="_blank">Twitch</a>
      </li>
      <li class="nav-item mr-auto">
        <a class="nav-link menuLink" href="https://www.youtube.com/channel/UCxrQlpG99emYZARkYT1bsVQ" target="_blank">Youtube</a>
      </li>
      <li class="nav-item mr-auto">
        <a class="nav-link menuLink" href="https://twitter.com/Baka_Kamome" target="_blank">Twitter</a>
      </li>
    </ul>
  </div>
</nav>