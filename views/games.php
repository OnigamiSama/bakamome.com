
<h2 class="mx-auto text-center mt-5 mb-5 text-white"> Streamed Games </h2>
<div class=" mt-5 row mx-auto col col-md col-lg col-xl">

	<div class="card cardSize mx-auto mb-3 col-11 col-md-8 col-lg-5 col-xl-4 cardBg" style="width: 18rem;">
	  <img class="card-img-top" src="imgs/cards/ff14.jpg" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center">Final Fantasy 14</p>
	  </div>
	</div>
	<div class="card cardSize mx-auto mb-3 col-11 col-md-8 col-lg-5 col-xl-4" style="width: 18rem;">
	  <img class="card-img-top" src="imgs/cards/overwatch.png" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center">OverWatch</p>
	  </div>
	</div>
	<div class="card cardSize mx-auto mb-3 col-11 col-md-8 col-lg-5 col-xl-4" style="width: 18rem;">
	  <img class="card-img-top" src="imgs/cards/wow.jpg" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center">World of Warcraft</p>
	  </div>
	</div>
</div>

	

