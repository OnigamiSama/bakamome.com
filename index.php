
<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Extrait dans les partages -->
    <meta name="description" content="Bakamome Live Stream">
    <meta name="author" content="Maxime Stucki">

    <!-- meta tags pour les partages sur réseaux sociaux -->

    <meta property="og:title" content="Bakamome.com">
    <!-- Extrait dans les partages -->
    <meta property="og:description" content="Bakamome Live Stream">
    <meta property="og:image" content="https://bakamome.com/imgs/vignette.png">
    <meta property="og:url" content="https://bakamome.com/">
    <meta name="twitter:card" content="summary_large_image">

    <!-- meta tags pour les partages sur réseaux sociaux - non essentiels -->

    <meta property="og:site_name" content="https://Bakamome.com/">
    <meta name="twitter:image:alt" content="https://Bakamome.com/">

    <!-- Icon dans le navigateur du site -->
    <link rel="icon" type="image/png" href="imgs/mascotte.png" />
    
    <!-- Bootstrap4 CSS Javascript-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Icon divers a placer sur le site -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    
    <!-- Dev Own CSS -->
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="style/embedStream.css">
    


    <!-- titre de la page (en haut du navigateur) -->
    <title>Bakamome Live Stream</title>

  </head>
  
  <body id="homePage">
  
  	<!-- menu -->
  	<?php
    	include 'views/menu.php';
    ?>
  	
  	<!-- Corp de la page -->
  	<?php
    	include 'views/embedStream.php';
    ?>

  	<!-- footer -->
  	<?php
    	include 'views/footer.php';
    ?>
  </body>

</html> 