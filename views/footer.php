<div class="footer mx-auto d-flex flex-row-reverse mt-5">
	<a class="nav-link menuLink" href="https://twitter.com/Baka_Kamome" target="_blank"><i class="fab fa-twitter bakamomeColor"></i></a>
	<a class="nav-link menuLink" href="https://www.twitch.tv/bakamome" target="_blank"><i class="fab fa-twitch bakamomeColor"></i></a>
	<a class="nav-link menuLink" href="https://www.youtube.com/channel/UCxrQlpG99emYZARkYT1bsVQ" target="_blank"><i class="fab fa-youtube bakamomeColor"></i></a>
</div>